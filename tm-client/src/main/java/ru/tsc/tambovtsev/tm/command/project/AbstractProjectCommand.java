package ru.tsc.tambovtsev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    public String getArgument() {
        return null;
    }

    public void renderProjects(@Nullable final List<ProjectDTO> projects) {
        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @NotNull
    protected IProjectEndpoint getTaskEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return Role.values();
    }

    public void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        @Nullable final Status status = project.getStatus();
        System.out.println("STATUS: " + status.getDisplayName());
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

}

