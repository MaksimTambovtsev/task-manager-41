package ru.tsc.tambovtsev.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.Domain;
import ru.tsc.tambovtsev.tm.dto.request.DataXmlSaveJaxBRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DomainSaveXMLJaxbCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "save-xml-jaxb";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataXmlJaxB(new DataXmlSaveJaxBRequest(getToken()));
    }

}
