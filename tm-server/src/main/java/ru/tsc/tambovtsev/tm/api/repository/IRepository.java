package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

import java.util.Collection;

public interface IRepository<M extends AbstractEntityDTO> {

    @Nullable
    M findById(@Nullable String id);

    @Nullable
    void removeById(@Nullable String id);

    void addAll(@NotNull Collection<M> models);

}
