package ru.tsc.tambovtsev.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

import java.util.*;

import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
import static org.mybatis.dynamic.sql.SqlBuilder.select;
import static ru.tsc.tambovtsev.tm.dynamicsql.provider.TaskProvider.*;

public class TaskService extends AbstractUserOwnedService<TaskDTO, ITaskRepository> implements ITaskService {

    public TaskService(
            @Nullable final ITaskRepository repository,
            @NotNull IConnectionService connection
    ) {
        super(repository, connection);
    }

    @NotNull
    @Override
    public ITaskRepository getRepository(@NotNull SqlSession connection) {
        return connection.getMapper(ITaskRepository.class);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setUserId(userId);
            task.setName(name);
            task.setDescription(description);
            repository.create(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final TaskDTO task = repository.findById(userId, id);
            if (task == null) return null;
            task.setName(name);
            task.setDescription(description);
            task.setUserId(userId);
            repository.updateById(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            final TaskDTO task = repository.findById(userId, id);
            if (task == null) return null;
            task.setStatus(status);
            task.setUserId(userId);
            repository.updateById(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e){
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            SqlColumn<?> sortColumn;
            if (sort.name().equals("NAME")){
                sortColumn = name;
            } else sortColumn = status;
            final SelectStatementProvider selectStatement =
                    select(id, userId, name, description, status, projectId).
                            from(task).
                            where(userId, isEqualTo(userIdPr)).
                            orderBy(sortColumn).
                            build().
                            render(RenderingStrategies.MYBATIS3);
            @Nullable final List<TaskDTO> result = repository.findAll(selectStatement);
            return result;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
        try {
            @Nullable final List<TaskDTO> result = repository.findAllTask();
            return result;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clearTask() {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clearTask();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@NotNull final Collection<TaskDTO> models) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        @NotNull final SqlSession sqlSession = connection.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.clearTask();
            repository.addAll(models);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return models;
    }

}
