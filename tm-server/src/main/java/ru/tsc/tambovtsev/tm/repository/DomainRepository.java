package ru.tsc.tambovtsev.tm.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.IDomainRepository;
import ru.tsc.tambovtsev.tm.api.service.IServiceLocator;
import ru.tsc.tambovtsev.tm.dto.Domain;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DomainRepository implements IDomainRepository {

    @NotNull
    private final IServiceLocator serviceLocator;

    @NotNull
    private static final String CATALOG_DATA = "dump";

    @NotNull
    private static final String FILE_BINARY = "data.bin";

    @NotNull
    private static final String FILE_BACkUP = "backup.base64";

    @NotNull
    private static final String FILE_BASE64 = "data.base64";

    @NotNull
    private static final String FILE_JAXB_XML = "data.jaxb.xml";

    @NotNull
    private static final String FILE_JAXB_JSON = "data.jaxb.json";

    @NotNull
    private static final String FILE_FASTERXML_XML = "data.fasterxml.xml";

    @NotNull
    private static final String FILE_FASTERXML_JSON = "data.fasterxml.json";

    @NotNull
    private static final String FILE_FASTERXML_YAML = "data.fasterxml.yaml";

    @NotNull
    private static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    private static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    private static final String APPLICATION_JSON = "application/json";

    public DomainRepository(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    private Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    private void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
    }

    @NotNull
    @SneakyThrows
    private static Path getPathFile(@NotNull String fileName) {
        @NotNull final Path pathCatalog = Paths.get("./", CATALOG_DATA);
        @NotNull final Path pathFile = Paths.get("./", CATALOG_DATA, fileName);
        if (!Files.exists(pathCatalog)) Files.createDirectory(pathCatalog);
        return pathFile;
    }

    @Override
    @SneakyThrows
    public void loadDataBackup() {
        if (!Files.exists(Paths.get(FILE_BACkUP))) return;
        @NotNull final ObjectInputStream objectInputStream =
                     new ObjectInputStream(Files.newInputStream(getPathFile(FILE_BACkUP)));
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBackup() {
        try (@NotNull final ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(Files.newOutputStream(getPathFile(FILE_BACkUP)))) {
            objectOutputStream.writeObject(getDomain());
        }
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        @NotNull final byte[] base64Byte = Files.readAllBytes(getPathFile(FILE_BASE64));
        @Nullable final String base64 = new String(base64Byte);
        @Nullable final byte[] buffer = new BASE64Decoder().decodeBuffer(base64);
        try (@NotNull final ObjectInputStream objectInputStream =
                     new ObjectInputStream(new ByteArrayInputStream(buffer))) {
            @NotNull Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        }

    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @NotNull final String base64;
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try (@NotNull final ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(byteArrayOutputStream)) {
            objectOutputStream.writeObject(getDomain());
            base64 = new BASE64Encoder().encode(byteArrayOutputStream.toByteArray());
        }
        try (@NotNull OutputStream outputStream = Files.newOutputStream(getPathFile(FILE_BASE64))) {
            outputStream.write(base64.getBytes());
        }

    }

    @Override
    @SneakyThrows
    public void loadDataBinary() {
        try (@NotNull final ObjectInputStream objectInputStream =
                     new ObjectInputStream(Files.newInputStream(getPathFile(FILE_BINARY)))) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void saveDataBinary() {
        try (@NotNull final ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(Files.newOutputStream(getPathFile(FILE_BINARY)))) {
            objectOutputStream.writeObject(getDomain());
        }
    }

    @Override
    @SneakyThrows
    public void loadDataJsonFasterXml() {
        try (@NotNull final InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_FASTERXML_JSON))) {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final Domain domain = objectMapper.readValue(inputStream, Domain.class);
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void saveDataJsonFasterXml() {
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_FASTERXML_JSON))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            outputStream.write(json.getBytes());
        }
    }

    @Override
    @SneakyThrows
    public void loadDataJsonJaxb() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        try (@NotNull final InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_JAXB_JSON))) {
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(inputStream);
            setDomain(domain);
        }

    }

    @Override
    @SneakyThrows
    public void saveDataJsonJaxb() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_JAXB_JSON))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
            marshaller.marshal(domain, outputStream);
        }
    }

    @Override
    @SneakyThrows
    public void loadDataXmlJaxb() {
        try (@NotNull final InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_JAXB_XML))) {
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(inputStream);
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void saveDataXmlJaxb() {
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_JAXB_XML))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
            @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(domain, outputStream);
        }
    }

    @Override
    @SneakyThrows
    public void loadDataXmlFasterXml() {
        try (@NotNull final InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_FASTERXML_XML))) {
            @NotNull final XmlMapper xmlMapper = new XmlMapper();
            @NotNull final Domain domain = xmlMapper.readValue(inputStream, Domain.class);
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void saveDataXmlFasterXml() {
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_FASTERXML_XML))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final XmlMapper xmlMapper = new XmlMapper();
            @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            outputStream.write(xml.getBytes());
        }
    }

    @Override
    @SneakyThrows
    public void loadDataYamlFasterXml() {
        try (@NotNull final InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_FASTERXML_YAML))) {
            @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
            @NotNull final Domain domain = yamlMapper.readValue(inputStream, Domain.class);
            setDomain(domain);
        }
    }

    @Override
    @SneakyThrows
    public void saveDataYamlFasterXml() {
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_FASTERXML_YAML))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
            @NotNull final String yaml = yamlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            outputStream.write(yaml.getBytes());
        }
    }

}
