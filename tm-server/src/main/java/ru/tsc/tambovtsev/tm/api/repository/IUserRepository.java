package ru.tsc.tambovtsev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.handler.RoleTypeHandler;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Insert("INSERT INTO TM_USER (ID, LOGIN, EMAIL, PASSWORD_HASH, FIRST_NAME, LAST_NAME, MIDDLE_NAME, ROLE, LOCKED) VALUES " +
            "(#{id}, #{login}, #{email}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{role}, 0)"
    )
    void addAll(@NotNull Collection<UserDTO> models);

    @Insert("INSERT INTO TM_USER (ID, LOGIN, EMAIL, PASSWORD_HASH, FIRST_NAME, LAST_NAME, MIDDLE_NAME, ROLE, LOCKED) VALUES " +
    "(#{id}, #{login}, #{email}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{role}, 0)"
    )
    void create(@Nullable UserDTO user);

    @Update("UPDATE TM_USER SET LOCKED = 1 WHERE ID = #{id}")
    void lockUserById(@Param ("id") @NotNull String id);

    @Update("UPDATE TM_USER SET LOCKED = 0 WHERE ID = #{id}")
    void unlockUserById(@Param ("id") @NotNull String id);

    @Update(
            "UPDATE TM_USER SET " +
                    "FIRST_NAME = #{firstName}, LAST_NAME = #{lastName}, MIDDLE_NAME = #{middleName}, " +
                    "PASSWORD_HASH = #{passwordHash} WHERE ID = #{id}"
    )
    void updateUser(@NotNull UserDTO user);

    @Nullable
    @Select("SELECT * FROM TM_USER WHERE LOGIN = #{login}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    UserDTO findByLogin(@Param ("login") @Nullable String login);

    @Nullable
    @Select("SELECT * FROM TM_USER WHERE EMAIL = #{email}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    UserDTO findByEmail(@Param ("email") @Nullable String email);

    @Delete("DELETE FROM TM_USER WHERE LOGIN = #{login}")
    void removeByLogin(@Param ("login") @Nullable String login);

    @Nullable
    @Select("SELECT * FROM TM_USER WHERE ID = #{id}")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    UserDTO findById(@Param ("id") @Nullable String id);

    @Nullable
    @Select("SELECT * FROM TM_USER")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "login", column = "LOGIN"),
            @Result(property = "email", column = "EMAIL"),
            @Result(property = "passwordHash", column = "PASSWORD_HASH"),
            @Result(property = "firstName", column = "FIRST_NAME"),
            @Result(property = "lastName", column = "LAST_NAME"),
            @Result(property = "middleName", column = "MIDDLE_NAME"),
            @Result(property = "role", column = "ROLE", typeHandler = RoleTypeHandler.class),
            @Result(property = "locked", column = "LOCKED"),
    })
    List<UserDTO> findAllUser();

    @Delete("DELETE FROM TM_USER")
    void clearUser();

}
