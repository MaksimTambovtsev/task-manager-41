package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    void clearProject();

    @Nullable
    List<ProjectDTO> findAll();

}
