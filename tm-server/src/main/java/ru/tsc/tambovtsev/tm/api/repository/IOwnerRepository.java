package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IOwnerRepository<M extends AbstractUserOwnedModelDTO> extends IRepository<M> {

    void clear(@Nullable String userId);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    int getSize(@Nullable String userId);

    @Nullable
    void removeById(@Nullable String userId, @Nullable String id);

}
