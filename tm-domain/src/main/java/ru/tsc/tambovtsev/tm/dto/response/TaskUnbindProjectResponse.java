package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskUnbindProjectResponse extends AbstractTaskResponse {

    public TaskUnbindProjectResponse(@Nullable TaskDTO task) {
        super(task);
    }

}
